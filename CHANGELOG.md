# Changelog

## [1.0.0] - 2024-05-20
### Added
  - Initial version was developed according to the issue [PESB-1753](https://jira.beeline.kz/browse/PESB-1753)
