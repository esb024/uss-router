package com.veon.microservices.esb.service.uss.router.service.impl

import com.veon.microservices.esb.service.uss.router.*
import com.veon.microservices.esb.service.uss.router.config.property.ApplicationProperties
import com.veon.microservices.esb.service.uss.router.constant.Constant.RU
import com.veon.microservices.esb.service.uss.router.constant.ResponseState
import com.veon.microservices.esb.service.uss.router.exception.IntegrationException
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.CdbApiService
import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.RpUserEquipmentService
import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.model.UserEquipment
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.SmsAggregatorService
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model.SendResponse
import com.veon.microservices.esb.service.uss.router.integration.ussd.UssdService
import com.veon.microservices.esb.service.uss.router.persistence.repository.SubscriberRepository
import com.veon.microservices.libs.esb.commons.kotlin.extension.jsonToObject
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK

class UssRouterServiceImplTests : AnnotationSpec() {

    @MockK
    private lateinit var properties: ApplicationProperties
    @MockK
    private lateinit var subscriberRepository: SubscriberRepository
    @MockK
    private lateinit var cdbApiService: CdbApiService
    @MockK
    private lateinit var rpUserEquipmentService: RpUserEquipmentService
    @MockK
    private lateinit var ussdService: UssdService
    @MockK
    private lateinit var smsAggregatorService: SmsAggregatorService
    @InjectMockKs
    private lateinit var service: UssRouterServiceImpl

    @BeforeEach
    fun init() {
        MockKAnnotations.init(this)
        properties.mockValues()
    }

    @AfterEach
    fun clearAll() {
        clearAllMocks()
    }

    @Test
    fun checkSubscriber() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   "
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns """
            {
                "devicetype": "Smartphone"
            }
        """.trimIndent().jsonToObject()

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.SUCCESS.value
    }

    @Test
    fun `checkSubscriber when existsById false`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns false

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun `checkSubscriber when subscriberData is null`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns null

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun `checkSubscriber when subscriberData soc is null`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": null
            }
        """.trimIndent().jsonToObject()

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun `checkSubscriber when subscriberData soc is not permitted`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "2EAPE   "
            }
        """.trimIndent().jsonToObject()

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun `checkSubscriber when userEquipment is null`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   "
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns null

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun `checkSubscriber when userEquipment deviceType is not Smartphone`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   "
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns """
            {
                "devicetype": null
            }
        """.trimIndent().jsonToObject()

        service.checkSubscriber(TEST_MSISDN) shouldBe ResponseState.FAILURE.value
    }

    @Test
    fun routeUss() {
        routeUss(RU, TEST_RU_SUCCESSFUL_MSG)
        routeUss("kz", TEST_KZ_SUCCESSFUL_MSG)
    }

    private fun routeUss(lang: String, expected: String) {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   ",
                "accountType": 13
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns """
            {
                "devicetype": "Smartphone"
            }
        """.trimIndent().jsonToObject()

        every {
            smsAggregatorService.send(TEST_MSISDN, lang)
        } returns """
            {
                "id": 1231231
            }
        """.trimIndent().jsonToObject()

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, lang) shouldBe expected

        verify(exactly = 0) {
            ussdService.routeUss(any(), any(), any())
        }
    }

    @Test
    fun `routeUss when existsById is false`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns false

        every {
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        } returns TEST_RU_SUCCESSFUL_MSG

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, RU) shouldBe TEST_RU_SUCCESSFUL_MSG

        verify(exactly = 0) {
            cdbApiService.getSubscriberData(any())
            rpUserEquipmentService.getUserEquipment(any())
            smsAggregatorService.send(any(), any())
        }
    }

    @Test
    fun `routeUss when subscriberData is invalid`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns null

        every {
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        } returns TEST_RU_SUCCESSFUL_MSG

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, RU) shouldBe TEST_RU_SUCCESSFUL_MSG

        verify(exactly = 0) {
            rpUserEquipmentService.getUserEquipment(any())
            smsAggregatorService.send(any(), any())
        }
    }

    @Test
    fun `routeUss when userEquipment is null`() {
        `routeUss when userEquipment is invalid`(null)
    }

    @Test
    fun `routeUss when userEquipment has invalid deviceType`() {
        `routeUss when userEquipment is invalid`("""
            {
                "devicetype": "device"
            }
        """.trimIndent().jsonToObject())
    }

    private fun `routeUss when userEquipment is invalid`(userEquipment: UserEquipment?) {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   ",
                "accountType": 113
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns userEquipment

        every {
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        } returns TEST_RU_SUCCESSFUL_MSG

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, RU) shouldBe TEST_RU_SUCCESSFUL_MSG

        verify(exactly = 0) {
            smsAggregatorService.send(any(), any())
        }
    }

    @Test
    fun `routeUss when smsAggregator returns null`() {
        `routeUss when smsAggregator returns invalid response`(null)
    }

    @Test
    fun `routeUss when smsAggregator returns null id`() {
        `routeUss when smsAggregator returns invalid response`(SendResponse())
    }

    private fun `routeUss when smsAggregator returns invalid response`(sendResponse: SendResponse?) {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } returns """
            {
                "soc": "3EAPE   ",
                "accountType": 113
            }
        """.trimIndent().jsonToObject()

        every {
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
        } returns """
            {
                "devicetype": "Smartphone"
            }
        """.trimIndent().jsonToObject()

        every {
            smsAggregatorService.send(TEST_MSISDN, RU)
        } returns sendResponse

        every {
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        } returns TEST_RU_SUCCESSFUL_MSG

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, RU) shouldBe TEST_RU_SUCCESSFUL_MSG

        verifySequence {
            subscriberRepository.existsById(TEST_MSISDN)
            cdbApiService.getSubscriberData(TEST_MSISDN)
            rpUserEquipmentService.getUserEquipment(TEST_MSISDN)
            smsAggregatorService.send(TEST_MSISDN, RU)
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        }
    }

    @Test
    fun `routeUss when integration exception is thrown`() {
        every {
            subscriberRepository.existsById(TEST_MSISDN)
        } returns true

        every {
            cdbApiService.getSubscriberData(TEST_MSISDN)
        } throws IntegrationException()

        every {
            ussdService.routeUss(TEST_MSISDN, TEST_BNUMBER, "kz")
        } returns TEST_KZ_SUCCESSFUL_MSG

        service.routeUss(TEST_MSISDN, TEST_BNUMBER, "kz") shouldBe TEST_KZ_SUCCESSFUL_MSG

        verify(exactly = 0) {
            rpUserEquipmentService.getUserEquipment(any())
            smsAggregatorService.send(any(), any())
        }
    }
}