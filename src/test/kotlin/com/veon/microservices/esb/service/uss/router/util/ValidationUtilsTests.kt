package com.veon.microservices.esb.service.uss.router.util

import com.veon.microservices.esb.service.uss.router.config.property.ApplicationProperties
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.model.SubscriberData
import com.veon.microservices.esb.service.uss.router.mockValues
import com.veon.microservices.esb.service.uss.router.util.ValidationUtils.isInvalidSubscriberDataForRouteUss
import com.veon.microservices.libs.esb.commons.kotlin.extension.jsonToObject
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK

class ValidationUtilsTests : AnnotationSpec() {

    @MockK
    private lateinit var applicationProperties: ApplicationProperties

    @BeforeAll
    fun beforeAll() {
        MockKAnnotations.init(this)
        applicationProperties.mockValues()
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss when valid`() {
        val response = """
            {
              "ban": 180000711,
              "subStatus": "S",
              "ben": 1,
              "soc": "M2M_NV   ",
              "accountType": 13,
              "marketCode": "KZT",
              "banStatus": "S",
              "hierarchyId": 0,
              "arpu": 0,
              "initActivationDate": null
            }
        """.trimIndent().jsonToObject<SubscriberData>()

        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe false
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid when response is null`() {
        isInvalidSubscriberDataForRouteUss(null, applicationProperties) shouldBe true
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid when soc is null`() {
        val response = """
            {
              "accountType": 113
            }
        """.trimIndent().jsonToObject<SubscriberData>()
        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe true
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid when accountType is null`() {
        val response = """
            {
              "soc": "2EAPE   "
            }
        """.trimIndent().jsonToObject<SubscriberData>()
        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe true
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid soc`() {
        val response = """
            {
              "soc": "2EAPE   ",
              "accountType": 113
            }
        """.trimIndent().jsonToObject<SubscriberData>()

        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe true
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid accountType`() {
        val response = """
            {
              "soc": "M2M_NV   ",
              "accountType": 5
            }
        """.trimIndent().jsonToObject<SubscriberData>()

        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe true
    }

    @Test
    fun `isInvalidSubscriberDataForRouteUss invalid soc and accountType`() {
        val response = """
            {
              "soc": "FTSOCIAL   ",
              "accountType": 131
            }
        """.trimIndent().jsonToObject<SubscriberData>()

        isInvalidSubscriberDataForRouteUss(response, applicationProperties) shouldBe true
    }
}
