package com.veon.microservices.esb.service.uss.router.integration.ussd.impl

import com.veon.microservices.esb.service.uss.router.TEST_BNUMBER
import com.veon.microservices.esb.service.uss.router.TEST_MSISDN
import com.veon.microservices.esb.service.uss.router.config.property.UssdProperties
import com.veon.microservices.esb.service.uss.router.constant.Constant.RU
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class UssdServiceImplTests : AnnotationSpec() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: UssdServiceImpl

    @BeforeAll
    fun beforeAll() {
        mockWebServer = MockWebServer()
        val url = mockWebServer.url("/").toUrl().toString()
        val properties = UssdProperties(url, "/uss/servlet/ussdHttpServlet.{lang}")
        service = UssdServiceImpl(WebClient.builder()
            .baseUrl(properties.url)
            .build(),
            properties)
    }

    @Test
    fun routeUss() {
        val responseBody = "Ваш запрос в деле :) Скоро прилетит SMS о выполнении! Ответ придет в виде СМС"
        mockWebServer.enqueue(
            MockResponse().apply {
                setResponseCode(200)
                setHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE)
                setBody(responseBody)
            }
        )
        val response = service.routeUss(TEST_MSISDN, TEST_BNUMBER, RU)
        response shouldBe responseBody
        val request = mockWebServer.takeRequest()
        request.method shouldBe "GET"
        request.path shouldBe "/uss/servlet/ussdHttpServlet.ru?ussd=$TEST_BNUMBER&msisdn=$TEST_MSISDN"
    }
}