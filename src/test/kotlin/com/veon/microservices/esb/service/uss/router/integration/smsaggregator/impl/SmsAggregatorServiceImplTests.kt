package com.veon.microservices.esb.service.uss.router.integration.smsaggregator.impl

import com.veon.microservices.esb.service.uss.router.TEST_MSISDN
import com.veon.microservices.esb.service.uss.router.config.property.LanguageSettings
import com.veon.microservices.esb.service.uss.router.config.property.SmsAggregatorProperties
import com.veon.microservices.esb.service.uss.router.constant.Constant.RU
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model.SendRequest
import com.veon.microservices.libs.esb.commons.kotlin.extension.jsonToObject
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.mockk.MockKAnnotations
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class SmsAggregatorServiceImplTests : AnnotationSpec() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: SmsAggregatorServiceImpl

    @BeforeAll
    fun beforeAll() {
        MockKAnnotations.init(this)
        mockWebServer = MockWebServer()
        val url = mockWebServer.url("/").toUrl().toString()
        val properties = SmsAggregatorProperties(url, "/api/v3/send",
            LanguageSettings("TEMPLATE_RU","TEMPLATE_KZ"),
            "Beeline")
        service = SmsAggregatorServiceImpl(WebClient.builder()
            .baseUrl(properties.url)
            .build(),
            properties)
    }

    @Test
    fun send() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setResponseCode(200)
                setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                setBody("""
                    {
                        "id": 123124
                    }
                """.trimIndent())
            }
        )
        val response = service.send(TEST_MSISDN, RU)
        response shouldNotBe null
        response.id shouldBe 123124L
        val request = mockWebServer.takeRequest()
        request.method shouldBe "POST"
        request.path shouldBe "/api/v3/send"
        val sendRequest = request.body.readUtf8().jsonToObject<SendRequest>()
        sendRequest.sender shouldBe "Beeline"
        sendRequest.phone shouldBe TEST_MSISDN
        sendRequest.templateId shouldBe "TEMPLATE_RU"
    }

    @Test
    fun send_kz() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setResponseCode(200)
                setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                setBody("""
                    {
                        "id": 123124
                    }
                """.trimIndent())
            }
        )
        val response = service.send(TEST_MSISDN, "kz")
        response shouldNotBe null
        response.id shouldBe 123124L
        val request = mockWebServer.takeRequest()
        request.method shouldBe "POST"
        request.path shouldBe "/api/v3/send"
        val sendRequest = request.body.readUtf8().jsonToObject<SendRequest>()
        sendRequest.sender shouldBe "Beeline"
        sendRequest.phone shouldBe TEST_MSISDN
        sendRequest.templateId shouldBe "TEMPLATE_KZ"
    }
}