package com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.impl

import com.veon.microservices.esb.service.uss.router.TEST_MSISDN
import com.veon.microservices.esb.service.uss.router.config.property.RpUserEquipmentProperties
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient


class RpUserEquipmentServiceImplTests : AnnotationSpec() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: RpUserEquipmentServiceImpl

    @BeforeAll
    fun beforeAll() {
        mockWebServer = MockWebServer()
        val url = mockWebServer.url("/").toUrl().toString()
        val properties = RpUserEquipmentProperties(url,
            "/api/subscribers/{msisdn}/userEquipment")
        service = RpUserEquipmentServiceImpl(WebClient.builder()
            .baseUrl(properties.url)
            .build(),
            properties)
    }

    @Test
    fun getUserEquipment() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setResponseCode(200)
                setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                setBody("""
                    {
                        "devicetype": "SMARTPHONE"
                    }
                """.trimIndent())
            }
        )
        service.getUserEquipment(TEST_MSISDN)
        val webServerRequest = mockWebServer.takeRequest()
        webServerRequest.method shouldBe "GET"
        webServerRequest.path shouldBe "/api/subscribers/$TEST_MSISDN/userEquipment?attributes=devicetype"
    }
}