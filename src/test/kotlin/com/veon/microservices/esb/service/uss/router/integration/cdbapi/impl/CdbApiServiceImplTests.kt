package com.veon.microservices.esb.service.uss.router.integration.cdbapi.impl

import com.veon.microservices.esb.service.uss.router.TEST_MSISDN
import com.veon.microservices.esb.service.uss.router.config.property.CdbApiProperties
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.matchers.shouldBe
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class CdbApiServiceImplTests : AnnotationSpec() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var service: CdbApiServiceImpl


    @BeforeAll
    fun beforeAll() {
        mockWebServer = MockWebServer()
        val url = mockWebServer.url("/").toUrl().toString()
        val properties = CdbApiProperties(url, "/subscriber/data")
        service = CdbApiServiceImpl(WebClient.builder()
            .baseUrl(properties.url)
            .build(),
            properties)
    }

    @Test
    fun `getSubscriberData success`() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setResponseCode(200)
                setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                setBody("""
                    {
                      "ban": 180000711,
                      "subStatus": "S",
                      "ben": 1,
                      "soc": "M2M_NV   ",
                      "accountType": 13,
                      "marketCode": "KZT",
                      "banStatus": "S",
                      "hierarchyId": 0,
                      "arpu": 0,
                      "initActivationDate": null
                    }
                """.trimIndent())
            }
        )
        service.getSubscriberData(TEST_MSISDN)
        val webServerRequest = mockWebServer.takeRequest()
        webServerRequest.method shouldBe "GET"
        webServerRequest.path shouldBe "/subscriber/data?msisdn=$TEST_MSISDN"
    }
}