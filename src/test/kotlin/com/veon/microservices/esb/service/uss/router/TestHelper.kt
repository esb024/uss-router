package com.veon.microservices.esb.service.uss.router

import com.veon.microservices.esb.service.uss.router.config.property.ApplicationProperties
import com.veon.microservices.esb.service.uss.router.config.property.LanguageSettings
import io.mockk.every

const val TEST_MSISDN = "7771112233"
const val TEST_BNUMBER = "123"
const val TEST_RU_SUCCESSFUL_MSG = "Ваш запрос в деле :) Скоро прилетит SMS о выполнении! Ответ придет в виде СМС"
const val TEST_KZ_SUCCESSFUL_MSG = "Сауалыңыз өңделуде :) Орындалғаны туралы SMS қазір келеді!"

fun ApplicationProperties.mockValues() {
    every { socList } returns setOf("FTSOCIAL","2EAPE")
    every { accountTypeList } returns setOf(13, 113, 131)
    every { successfulMessage } returns LanguageSettings(TEST_RU_SUCCESSFUL_MSG, TEST_KZ_SUCCESSFUL_MSG)
}
