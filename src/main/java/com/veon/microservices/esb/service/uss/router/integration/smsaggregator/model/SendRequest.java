package com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SendRequest {

  private String phone;
  private String templateId;
  private String sender;
}
