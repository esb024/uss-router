package com.veon.microservices.esb.service.uss.router.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constant {

  public static final String SMARTPHONE = "Smartphone";
  public static final String RU = "ru";
}
