package com.veon.microservices.esb.service.uss.router.persistence.repository;

import com.veon.microservices.esb.service.uss.router.persistence.entity.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber, String> {

}
