package com.veon.microservices.esb.service.uss.router.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IntegrationException extends RuntimeException {

  public IntegrationException(Throwable e) {
    super(e);
  }
}
