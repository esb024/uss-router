package com.veon.microservices.esb.service.uss.router.config.property;

import jakarta.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application.integration.cdb-api")
public record CdbApiProperties(@NotNull String url, @NotNull String subscriberDataPath) {
}
