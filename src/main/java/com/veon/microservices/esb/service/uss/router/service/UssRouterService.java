package com.veon.microservices.esb.service.uss.router.service;

public interface UssRouterService {

  String checkSubscriber(String msisdn);

  String routeUss(String msisdn, String bnum, String lang);
}
