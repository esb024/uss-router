package com.veon.microservices.esb.service.uss.router.exception;

import com.veon.microservices.esb.service.uss.router.constant.ResponseState;
import jakarta.validation.ConstraintViolationException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class UssRouterControllerExceptionHandler {

  @ExceptionHandler(IntegrationException.class)
  public ResponseEntity<String> integrationException(IntegrationException e) {
    log.error("Integration exception", e);
    return ResponseEntity.ok()
        .contentType(MediaType.TEXT_PLAIN)
        .body(ResponseState.FAILURE.getValue());
  }

  @ExceptionHandler(CannotCreateTransactionException.class)
  public ResponseEntity<String> databaseException(CannotCreateTransactionException e) {
    log.error("Database exception", e);
    return ResponseEntity.ok()
        .contentType(MediaType.TEXT_PLAIN)
        .body(ResponseState.FAILURE.getValue());
  }

  @ExceptionHandler({
      ConstraintViolationException.class,
      MethodArgumentNotValidException.class,
      MissingServletRequestParameterException.class
  })
  public ResponseEntity<Map<String, String>> validationException(Exception e) {
    log.error("Validation exception", e);
    return ResponseEntity.badRequest()
        .body(Map.of("message", "Request validation error"));
  }
}
