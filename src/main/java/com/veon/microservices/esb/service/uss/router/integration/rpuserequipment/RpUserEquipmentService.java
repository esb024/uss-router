package com.veon.microservices.esb.service.uss.router.integration.rpuserequipment;

import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.model.UserEquipment;

public interface RpUserEquipmentService {

  UserEquipment getUserEquipment(String msisdn);
}
