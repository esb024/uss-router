package com.veon.microservices.esb.service.uss.router.config.property;

import java.util.Set;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application")
public record ApplicationProperties(Set<String> socList,
                                    Set<Integer> accountTypeList,
                                    LanguageSettings successfulMessage) {

}
