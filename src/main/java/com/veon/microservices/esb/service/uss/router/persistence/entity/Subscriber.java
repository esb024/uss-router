package com.veon.microservices.esb.service.uss.router.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "subscribers")
public class Subscriber {

  @Id
  @Column(length = 10, nullable = false)
  private String msisdn;
}
