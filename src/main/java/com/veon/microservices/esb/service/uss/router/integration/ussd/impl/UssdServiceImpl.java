package com.veon.microservices.esb.service.uss.router.integration.ussd.impl;

import com.veon.microservices.esb.service.uss.router.config.property.UssdProperties;
import com.veon.microservices.esb.service.uss.router.exception.IntegrationException;
import com.veon.microservices.esb.service.uss.router.integration.ussd.UssdService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Service
@RequiredArgsConstructor
public class UssdServiceImpl implements UssdService {

  private final WebClient ussdWebClient;
  private final UssdProperties properties;

  @Override
  public String routeUss(String msisdn, String bnumber, String lang) {
    return ussdWebClient.get()
        .uri(uriBuilder -> uriBuilder.path(properties.path())
            .queryParam("ussd", bnumber)
            .queryParam("msisdn", msisdn)
            .build(lang))
        .retrieve()
        .bodyToMono(String.class)
        .onErrorMap(IntegrationException::new)
        .block();
  }
}
