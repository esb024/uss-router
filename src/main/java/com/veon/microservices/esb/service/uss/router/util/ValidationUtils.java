package com.veon.microservices.esb.service.uss.router.util;

import com.veon.microservices.esb.service.uss.router.config.property.ApplicationProperties;
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.model.SubscriberData;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationUtils {

  public static boolean isInvalidSubscriberDataForRouteUss(SubscriberData response,
      ApplicationProperties properties) {
    return response == null || response.getSoc() == null
        || response.getAccountType() == null
        || !(!properties.socList().contains(response.getSoc().trim())
        && properties.accountTypeList().contains(response.getAccountType()));
  }
}
