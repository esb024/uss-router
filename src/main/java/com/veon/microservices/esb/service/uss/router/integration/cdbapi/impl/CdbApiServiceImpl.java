package com.veon.microservices.esb.service.uss.router.integration.cdbapi.impl;

import com.veon.microservices.esb.service.uss.router.config.property.CdbApiProperties;
import com.veon.microservices.esb.service.uss.router.exception.IntegrationException;
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.CdbApiService;
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.model.SubscriberData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Service
@RequiredArgsConstructor
public class CdbApiServiceImpl implements CdbApiService {

  private final WebClient cdbApiWebClient;
  private final CdbApiProperties properties;

  @Override
  public SubscriberData getSubscriberData(String msisdn) {
    return cdbApiWebClient.get()
        .uri(uriBuilder -> uriBuilder.path(properties.subscriberDataPath())
            .queryParam("msisdn", msisdn)
            .build())
        .retrieve()
        .bodyToMono(SubscriberData.class)
        .onErrorMap(IntegrationException::new)
        .block();
  }
}
