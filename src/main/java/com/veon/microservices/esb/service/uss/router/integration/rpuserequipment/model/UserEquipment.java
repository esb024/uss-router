package com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class UserEquipment {

  @JsonProperty("devicetype")
  private String deviceType;
}
