package com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class SendResponse {

  private Long id;
}
