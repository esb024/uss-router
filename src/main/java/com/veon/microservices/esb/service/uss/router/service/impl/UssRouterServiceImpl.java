package com.veon.microservices.esb.service.uss.router.service.impl;

import static com.veon.microservices.esb.service.uss.router.constant.Constant.RU;
import static com.veon.microservices.esb.service.uss.router.util.ValidationUtils.isInvalidSubscriberDataForRouteUss;

import com.veon.microservices.esb.service.uss.router.config.property.ApplicationProperties;
import com.veon.microservices.esb.service.uss.router.constant.Constant;
import com.veon.microservices.esb.service.uss.router.constant.ResponseState;
import com.veon.microservices.esb.service.uss.router.integration.cdbapi.CdbApiService;
import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.RpUserEquipmentService;
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.SmsAggregatorService;
import com.veon.microservices.esb.service.uss.router.integration.ussd.UssdService;
import com.veon.microservices.esb.service.uss.router.persistence.repository.SubscriberRepository;
import com.veon.microservices.esb.service.uss.router.service.UssRouterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UssRouterServiceImpl implements UssRouterService {

  private final ApplicationProperties properties;
  private final SubscriberRepository subscriberRepository;
  private final CdbApiService cdbApiService;
  private final RpUserEquipmentService rpUserEquipmentService;
  private final UssdService ussdService;
  private final SmsAggregatorService smsAggregatorService;

  @Override
  public String checkSubscriber(String msisdn) {

    log.info("checkSubscriber started, msisdn={}", msisdn);
    if (!subscriberRepository.existsById(msisdn)) {
      log.warn("no records were found in subscribers table, msisdn={}", msisdn);
      return ResponseState.FAILURE.getValue();
    }

    var subscriberData = cdbApiService.getSubscriberData(msisdn);
    if (subscriberData == null || subscriberData.getSoc() == null
        || properties.socList().contains(subscriberData.getSoc().trim())) {
      log.warn("cdb-api returned invalid subscriberData={}, msisdn={}", subscriberData, msisdn);
      return ResponseState.FAILURE.getValue();
    }

    var userEquipment = rpUserEquipmentService.getUserEquipment(msisdn);
    if (userEquipment != null && Constant.SMARTPHONE.equals(userEquipment.getDeviceType())) {
      log.info("checkSubscriber finished successfully, msisdn={}", msisdn);
      return ResponseState.SUCCESS.getValue();
    }
    log.warn("rp-user-equipment returned invalid data={}, msisdn={}", userEquipment, msisdn);
    return ResponseState.FAILURE.getValue();
  }

  @Override
  public String routeUss(String msisdn, String bnum, String lang) {
    try {
      log.info("routeUss started, msisdn={}", msisdn);
      if (!subscriberRepository.existsById(msisdn)) {
        log.warn("no records were found in subscribers table, msisdn={}, proxying to ussd",
            msisdn);
        return ussdService.routeUss(msisdn, bnum, lang);
      }

      var subscriberData = cdbApiService.getSubscriberData(msisdn);
      if (isInvalidSubscriberDataForRouteUss(subscriberData, properties)) {
        log.warn("cdb-api returned invalid subscriberData={}, msisdn={}, proxying to ussd",
            subscriberData, msisdn);
        return ussdService.routeUss(msisdn, bnum, lang);
      }

      var userEquipment = rpUserEquipmentService.getUserEquipment(msisdn);
      if (userEquipment != null && Constant.SMARTPHONE.equals(userEquipment.getDeviceType())) {
        var sendResponse = smsAggregatorService.send(msisdn, lang);
        if (sendResponse != null && sendResponse.getId() != null) {
          log.info("routeUss finished successfully, msisdn={}", msisdn);
          return RU.equals(lang) ? properties.successfulMessage().ru()
              : properties.successfulMessage().kz();
        } else {
          log.warn("sms-aggregator invalid sendResponse={}, msisdn={}, proxying to ussd",
              sendResponse, msisdn);
        }
      } else {
        log.warn("rp-user-equipment returned invalid data={}, msisdn={}, proxying to ussd",
            userEquipment, msisdn);
      }

      return ussdService.routeUss(msisdn, bnum, lang);
    } catch (Exception e) {
      log.error("Exception was thrown, proxying to ussd", e);
      return ussdService.routeUss(msisdn, bnum, lang);
    }
  }
}
