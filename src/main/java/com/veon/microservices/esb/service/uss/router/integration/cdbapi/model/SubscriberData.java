package com.veon.microservices.esb.service.uss.router.integration.cdbapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class SubscriberData {

  private String soc;
  private Integer accountType;
}
