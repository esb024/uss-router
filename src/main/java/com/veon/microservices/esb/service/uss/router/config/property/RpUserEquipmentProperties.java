package com.veon.microservices.esb.service.uss.router.config.property;

import jakarta.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application.integration.rp-user-equipment")
public record RpUserEquipmentProperties(@NotNull String url, @NotNull String userEquipmentPath) {

}
