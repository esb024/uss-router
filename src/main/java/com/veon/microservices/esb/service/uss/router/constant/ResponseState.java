package com.veon.microservices.esb.service.uss.router.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseState {
  SUCCESS("1"),
  FAILURE("2");

  private String value;
}
