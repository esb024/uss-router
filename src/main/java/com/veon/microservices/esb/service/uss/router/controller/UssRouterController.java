package com.veon.microservices.esb.service.uss.router.controller;

import com.veon.microservices.esb.service.uss.router.service.UssRouterService;
import com.veon.microservices.libs.validate.MsisdnFormatter;
import com.veon.microservices.libs.validate.constraint.msisdn.MSISDNConstraint;
import com.veon.microservices.libs.validate.constraint.msisdn.MSISDNFormatType;
import com.veon.microservices.libs.validate.constraint.regex.RegexConstraint;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api/ussrouter", produces = MediaType.TEXT_PLAIN_VALUE)
public class UssRouterController {

  private final UssRouterService service;

  @GetMapping(value = "checkSubscribers/{msisdn}")
  @Operation(summary = "Проверка тарифного плана, типа устройства абонента",
      operationId = "checkSubscribers")
  public String checkSubscriber(@PathVariable @MSISDNConstraint String msisdn) {
    return service.checkSubscriber(
        MsisdnFormatter.formatMSISDN(msisdn, MSISDNFormatType.DIGITS_10));
  }

  @GetMapping(value = "routeUSS/{msisdn}")
  @Operation(summary = "Проверка тарифного плана, типа аккаунта, типа устройства абонента, "
      + "отправка смс или роутинг в USSD",
      operationId = "routeUSS")
  public String routeUss(@PathVariable @MSISDNConstraint String msisdn,
      @RequestParam @NotBlank String bnum,
      @RequestParam(defaultValue = "ru")
      @RegexConstraint(patternPropPath = "application.lang-pattern") String lang) {
    return service.routeUss(MsisdnFormatter.formatMSISDN(msisdn, MSISDNFormatType.DIGITS_10),
        bnum, lang);
  }
}
