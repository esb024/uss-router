package com.veon.microservices.esb.service.uss.router.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

  private static final String CONTACT = "https://confluence.beeline.kz/display/ESB/ESB+Beeline+Kazakhstan";

  @Bean
  public OpenAPI openApi(@Value("${application.version}") String appVersion) {
    return new OpenAPI().info(
        new Info()
            .title("USS Router")
            .description("Закрытие USSD команд *111# *102# *106#")
            .version(appVersion)
            .contact(new Contact().name("Команда ESB Beeline Kazakhstan").url(CONTACT)));
  }
}
