package com.veon.microservices.esb.service.uss.router.integration.cdbapi;

import com.veon.microservices.esb.service.uss.router.integration.cdbapi.model.SubscriberData;

public interface CdbApiService {

  SubscriberData getSubscriberData(String msisdn);
}
