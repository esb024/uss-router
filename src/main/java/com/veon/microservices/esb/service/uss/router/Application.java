package com.veon.microservices.esb.service.uss.router;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages =
    { "com.veon.microservices.esb.service.uss.router.config" })
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }
}
