package com.veon.microservices.esb.service.uss.router.integration.smsaggregator.impl;

import static com.veon.microservices.esb.service.uss.router.constant.Constant.RU;

import com.veon.microservices.esb.service.uss.router.config.property.SmsAggregatorProperties;
import com.veon.microservices.esb.service.uss.router.exception.IntegrationException;
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.SmsAggregatorService;
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model.SendRequest;
import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model.SendResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Service
@RequiredArgsConstructor
public class SmsAggregatorServiceImpl implements SmsAggregatorService {

  private final WebClient smsAggregatorWebClient;
  private final SmsAggregatorProperties properties;

  @Override
  public SendResponse send(String msisdn, String lang) {
    var request = new SendRequest(msisdn,
        RU.equals(lang) ? properties.templateId().ru() : properties.templateId().kz(),
        properties.sender());
    return smsAggregatorWebClient.post()
        .uri(properties.sendPath())
        .bodyValue(request)
        .retrieve()
        .bodyToMono(SendResponse.class)
        .onErrorMap(IntegrationException::new)
        .block();
  }
}
