package com.veon.microservices.esb.service.uss.router.integration.smsaggregator;

import com.veon.microservices.esb.service.uss.router.integration.smsaggregator.model.SendResponse;

public interface SmsAggregatorService {

  SendResponse send(String msisdn, String lang);
}
