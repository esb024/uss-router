package com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.impl;

import com.veon.microservices.esb.service.uss.router.config.property.RpUserEquipmentProperties;
import com.veon.microservices.esb.service.uss.router.exception.IntegrationException;
import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.RpUserEquipmentService;
import com.veon.microservices.esb.service.uss.router.integration.rpuserequipment.model.UserEquipment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Service
@RequiredArgsConstructor
public class RpUserEquipmentServiceImpl implements RpUserEquipmentService {

  private final WebClient rpUserEquipmentWebClient;
  private final RpUserEquipmentProperties rpUserEquipmentProperties;

  @Override
  public UserEquipment getUserEquipment(String msisdn) {
    return rpUserEquipmentWebClient.get()
        .uri(urlBuilder -> urlBuilder.path(rpUserEquipmentProperties.userEquipmentPath())
            .queryParam("attributes","devicetype")
            .build(msisdn))
        .retrieve()
        .bodyToMono(UserEquipment.class)
        .onErrorMap(IntegrationException::new)
        .block();
  }
}
