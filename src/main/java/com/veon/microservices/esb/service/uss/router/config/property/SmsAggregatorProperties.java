package com.veon.microservices.esb.service.uss.router.config.property;

import jakarta.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application.integration.sms-aggregator")
public record SmsAggregatorProperties(@NotNull String url,
                                      @NotNull String sendPath,
                                      @NotNull LanguageSettings templateId,
                                      @NotNull String sender) {

}
