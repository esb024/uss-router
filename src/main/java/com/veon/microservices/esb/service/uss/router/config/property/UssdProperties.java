package com.veon.microservices.esb.service.uss.router.config.property;

import jakarta.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application.integration.ussd")
public record UssdProperties(@NotNull String url, @NotNull String path) {

}
