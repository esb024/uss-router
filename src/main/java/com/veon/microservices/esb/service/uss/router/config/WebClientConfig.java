package com.veon.microservices.esb.service.uss.router.config;

import com.veon.microservices.esb.service.uss.router.config.property.CdbApiProperties;
import com.veon.microservices.esb.service.uss.router.config.property.RpUserEquipmentProperties;
import com.veon.microservices.esb.service.uss.router.config.property.SmsAggregatorProperties;
import com.veon.microservices.esb.service.uss.router.config.property.UssdProperties;
import io.netty.handler.logging.LogLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class WebClientConfig {

  private final CdbApiProperties cdbApiProperties;
  private final RpUserEquipmentProperties rpUserEquipmentProperties;
  private final UssdProperties ussdProperties;
  private final SmsAggregatorProperties smsAggregatorProperties;

  @Value("${logging.level.reactor.netty.http.client}")
  private String httpClientLogLevel;

  @Bean
  public WebClient cdbApiWebClient(WebClient.Builder builder) {
    return getWebClientBuilder(builder)
        .baseUrl(cdbApiProperties.url())
        .build();
  }

  @Bean
  public WebClient rpUserEquipmentWebClient(WebClient.Builder builder) {
    return getWebClientBuilder(builder)
        .baseUrl(rpUserEquipmentProperties.url())
        .build();
  }

  @Bean
  public WebClient ussdWebClient(WebClient.Builder builder) {
    return getWebClientBuilder(builder)
        .baseUrl(ussdProperties.url())
        .build();
  }

  @Bean
  public WebClient smsAggregatorWebClient(WebClient.Builder builder) {
    return getWebClientBuilder(builder)
        .baseUrl(smsAggregatorProperties.url())
        .build();
  }

  private WebClient.Builder getWebClientBuilder(WebClient.Builder builder) {
    var httpClient = HttpClient.create()
        .wiretap("reactor.netty.http.client.HttpClient",
            getLogLevel(), AdvancedByteBufFormat.TEXTUAL);
    return builder
        .clientConnector(new ReactorClientHttpConnector(httpClient));
  }

  private LogLevel getLogLevel() {
    return LogLevel.valueOf(httpClientLogLevel.toUpperCase());
  }
}
