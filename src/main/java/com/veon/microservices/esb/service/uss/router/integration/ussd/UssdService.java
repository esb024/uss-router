package com.veon.microservices.esb.service.uss.router.integration.ussd;

public interface UssdService {
  String routeUss(String msisdn, String bnumber, String lang);
}
