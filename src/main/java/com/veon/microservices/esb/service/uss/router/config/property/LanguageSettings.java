package com.veon.microservices.esb.service.uss.router.config.property;

import jakarta.validation.constraints.NotNull;

public record LanguageSettings(@NotNull String ru, @NotNull String kz) {
  
}
