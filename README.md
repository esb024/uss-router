# USS Router

## Описание

- Интеграция:
  - Cdb API
  - RP User Equipment
  - SMS Aggregator
  - USSD
  - PostgreSQL

- Начальная задача: [PESB-1753](https://jira.beeline.kz/browse/PESB-1753)

## Описание методов

### checkSubscribers
____
#### Описание 
Проверка тарифного плана, типа устройства абонента
____
#### Пример вызова
_GET /api/ussrouter/checkSubscribers/{msisdn}_

HTTP 200 
Content-Type: text/plain

```text
2
```
____
### routeUSS
#### Описание
Проверка тарифного плана, типа аккаунта, типа устройства абонента, отправка смс или роутинг в USSD
____
#### Пример вызова
_GET /api/ussrouter/routeUSS/{msisdn}?bnum={bnum}&lang={lang}_

HTTP 200
Content-Type: text/plain

```text
Ваш запрос в деле :) Скоро прилетит SMS о выполнении! Ответ придет в виде СМС
```
