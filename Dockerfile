FROM docker-registry.beeline.kz/runtime/jre:17-temurin-alpine

EXPOSE 8080
EXPOSE 8090

HEALTHCHECK --interval=2m --timeout=10s --retries=3 --start-period=10s \
CMD wget -O- http://localhost:8090/actuator/health

ENV SPRING_CONFIG_NAME="application,system"

COPY ./build/libs/*.jar /opt/app.jar

ENTRYPOINT exec java -jar /opt/app.jar
